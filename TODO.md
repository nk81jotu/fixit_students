# Your Tasks #
This is a list of tasks to complete in order to understand software tests, service APIs, Continuous Integration (CI) and Continous Delivery (CD).

### Prerequisites ###

Go to the newly forked project and enable the CI runner "SWT pcai042 runner" at "Settings>CI/CD>Runner Setting>Specific Runners". Exchange the URLs of the first badge in the README and push these changes to Gitlab (see "Settings>CI/CD>General Pipeline Settings" for correct URLs). Wait for CI to finish and have a look at the job output.

Remember to push your changes after you've fulfilled each task.

**Problems:** If you can't setup CI, proceed with the tasks at your pc and notify the lecturer.

### Task 1 ###
Execute the following statement at the CLI and have a look at the output. Try to fix any issues!

`npm run lint`

### Task 2 ####

Execute the following statement at the CLI and have a look at the output. Try to fix all failing tests!

`npm run test:integration`

**Hint**: use `npm start` to run the application and head over to [http://localhost:3000/documentation](http://localhost:3000/documentation) in order to have a look at the API description and to test the API.

Add this step to the file `.gitlab-ci.yml`. CI will execute tests automatically in the future.

### Task 3 ###

Have a look at the code coverage by executing `npm run coverage`. Head to the `coverage` folder and search for the file `index.html`. Open it in a browser and dig through the code coverage report.

### Task 4 ###

Count your current Lines of Code (LOC) by executing `npm run countLOC:details`.

### Task 5 ###

Add tests for "delete basket items" in a separate file by copying `tests/integration_PostNewItem.js`. Subsequently modify these tests.

### Solutions ###
See branch "solution" at the [repository](https://git.informatik.uni-leipzig.de/meissner/fixit_students).
